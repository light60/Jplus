package com.jplus.framework.mvc;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.framework.bean.BeanHandle;
import com.jplus.framework.core.fault.InitializationError;
import com.jplus.framework.mvc.action.Handler;
import com.jplus.framework.mvc.action.Requester;
import com.jplus.framework.mvc.annotation.Controller;
import com.jplus.framework.mvc.annotation.Request;
import com.jplus.framework.util.FormatUtil;

/**
 * ActionHandle
 * 
 * @author Yuanqy
 *
 */
public class ActionHandle {
	private static final Logger logger = LoggerFactory.getLogger(ActionHandle.class);
	private static final Map<Requester, Handler> actionMap = new LinkedHashMap<Requester, Handler>();
	static {
		try {
			Set<Class<?>> beanSet = BeanHandle.beanSet;
			for (Class<?> cls : beanSet) {
				if (cls.isAnnotationPresent(Controller.class)) {
					// 遍历 Action 类
					Method[] actionMethods = cls.getDeclaredMethods();
					for (Method actionMethod : actionMethods) {
						handleActionMethod(cls, actionMethod);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Action 初始化失败：", e);
			throw new InitializationError(e);
		}
	}

	private static void handleActionMethod(Class<?> actionClass, Method actionMethod) {
		// 判断当前 Action 方法是否带有 Request 注解
		String actionPath = "", requestMethod = null;
		if (actionMethod.isAnnotationPresent(Request.Get.class)) {
			actionPath += actionMethod.getAnnotation(Request.Get.class).value();
			requestMethod = "GET";
		} else if (actionMethod.isAnnotationPresent(Request.Post.class)) {
			actionPath += actionMethod.getAnnotation(Request.Post.class).value();
			requestMethod = "POST";
		} else if (actionMethod.isAnnotationPresent(Request.Put.class)) {
			actionPath += actionMethod.getAnnotation(Request.Put.class).value();
			requestMethod = "PUT";
		} else if (actionMethod.isAnnotationPresent(Request.Delete.class)) {
			actionPath += actionMethod.getAnnotation(Request.Delete.class).value();
			requestMethod = "DELETE";
		}
		if (FormatUtil.isEmpty(actionPath) && requestMethod == null)
			return;
		Request.All all = actionClass.getAnnotation(Request.All.class);
		if (all != null)
			actionPath = all.value() + actionPath;
		actionPath = FormatUtil.formatPath(actionPath);
		actionMethod.setAccessible(true); // 取消类型安全检测（可提高反射性能）
		addActionMap(new Requester(requestMethod, actionPath), new Handler(actionClass, actionMethod, actionPath));
	}

	/**
	 * 手动追加Action
	 */
	public static void addActionMap(Requester req, Handler hand) {
		logger.info("\tAction[{}]:[{}]\t{}.{}()", req.getRequestMethod(), req.getRequestPath(), hand.getActionClass().getName(), hand.getActionMethod().getName());
		actionMap.put(req, hand);
	}

	/**
	 * 获取 Action Map
	 */
	public static Map<Requester, Handler> getActionMap() {
		return actionMap;
	}
}
