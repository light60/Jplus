package com.jplus.framework.mvc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jplus.framework.bean.annotation.Component;

/**
 * 定义请求
 *
 * @author huangyong
 * @author Yuanqy
 */
@Retention(RetentionPolicy.RUNTIME)
@Component
// 用于被框架扫描
public @interface Request {

	/**
	 * 定义公共头请求
	 *
	 * @author huangyong
	 * @since 2.1
	 */
	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface All {

		String value();
	}

	/**
	 * 定义 GET 请求
	 *
	 * @author huangyong
	 * @since 2.1
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Get {

		String value();
	}

	/**
	 * 定义 POST 请求
	 *
	 * @author huangyong
	 * @since 2.1
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Post {

		String value();
	}

	/**
	 * 定义 PUT 请求
	 *
	 * @author huangyong
	 * @since 2.1
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Put {

		String value();
	}

	/**
	 * 定义 DELETE 请求
	 *
	 * @author huangyong
	 * @since 2.1
	 */
	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Delete {

		String value();
	}
}
