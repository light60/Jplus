package com.jplus.framework.mvc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Form表单<br>
 * Pojo类添加此注解，表示该类视为Form表单，可作为Action的方法入参传递。
 * 
 * @author Yuanqy
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Form {
}
