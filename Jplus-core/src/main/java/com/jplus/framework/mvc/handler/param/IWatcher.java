package com.jplus.framework.mvc.handler.param;

import java.util.List;

import com.jplus.framework.mvc.action.Handler;

public interface IWatcher {
	public List<Object> getParams(Handler handler) throws Exception;
}
