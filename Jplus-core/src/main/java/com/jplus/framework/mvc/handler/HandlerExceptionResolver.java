package com.jplus.framework.mvc.handler;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.framework.AppConstant;
import com.jplus.framework.bean.annotation.Component;
import com.jplus.framework.mvc.WebUtil;
import com.jplus.framework.mvc.fault.AuthcException;
import com.jplus.framework.mvc.fault.AuthzException;

/**
 * 默认 Handler 异常解析器[单例]
 *
 * @author huangyong
 */
@Component
public class HandlerExceptionResolver {

	private static final Logger logger = LoggerFactory.getLogger(HandlerExceptionResolver.class);

	public void resolveHandlerException(Exception e) {
		// 判断异常原因
		Throwable cause = e.getCause();
		if (cause == null) {
			logger.error(e.getMessage(), e);
			return;
		}
		if (cause instanceof AuthcException) {
			// 分两种情况进行处理
			if (WebUtil.isAJAX()) {
				// 跳转到 403 页面
				WebUtil.sendError(HttpServletResponse.SC_FORBIDDEN, "");
			} else {
				// 重定向到首页
				WebUtil.redirectRequest(AppConstant.CONFIG.AppHomePage.getValue());
			}
		} else if (cause instanceof AuthzException) {
			// 跳转到 403 页面
			WebUtil.sendError(HttpServletResponse.SC_FORBIDDEN, "");
		} else {
			// 跳转到 500 页面
			WebUtil.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, cause.getMessage());
		}
	}
}
