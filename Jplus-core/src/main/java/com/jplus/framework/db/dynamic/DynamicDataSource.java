package com.jplus.framework.db.dynamic;

import java.util.Map;

import javax.sql.DataSource;

/**
 * 动态数据源配置<br/>
 * DynamicDataSource可以配置N个数据源，在数据库操作中可以做到动态切换<br/>
 * 但在事物中，无法切换[切换无效]。<br/>
 * 
 * @author Yuanqy
 *
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

	public DynamicDataSource(Map<String, DataSource> targetDataSources) {
		super.setTargetDataSources(targetDataSources);
		super.init();
	}

	/**
	 * 设置默认数据源
	 */
	@Override
	public void setDefaultTargetDataSource(DataSource defaultTargetDataSource) {
		super.setDefaultTargetDataSource(defaultTargetDataSource);
	}

	/**
	 * 如果没有指定数据源，是否使用默认数据源
	 */
	@Override
	public void setLenientFallback(boolean lenientFallback) {
		super.setLenientFallback(lenientFallback);
	}

	@Override
	public String determineCurrentLookupKey() {
		return DataSourceHolder.getDbType();
	}

}