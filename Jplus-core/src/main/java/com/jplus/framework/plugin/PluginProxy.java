package com.jplus.framework.plugin;

import java.util.List;

import com.jplus.framework.aop.proxy.Proxy;

/**
 * 插件代理
 *
 * @author huangyong
 * @since 2.0
 */
public abstract class PluginProxy implements Proxy {

	public abstract List<Class<?>> getTargetClassList();
}
