package com.jplus.framework.util;

class ClockUtil {
	private long rate = 0;// 频率
	private volatile long now = 0;// 当前时间

	private ClockUtil(long rate) {
		this.rate = rate;
		this.now = System.currentTimeMillis();
		start();
	}

	private void start() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(rate);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				now = System.currentTimeMillis();
			}
		}).start();
	}

	public long now() {
		return now;
	}

	public static final ClockUtil CLOCK = new ClockUtil(10);
}