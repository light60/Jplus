package com.jplus.plugins.druid;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSource;
import com.jplus.framework.InstanceFactory;
import com.jplus.framework.core.ConfigHandle;
import com.jplus.framework.db.DataSourceFactory;
import com.jplus.framework.plugin.Plugin;
/**
 * DruidPlugin
 * @author yuanqy
 *
 */
public class DruidPlugin extends DataSourceFactory implements Plugin {

	private DruidDataSource dataSource;

	@Override
	public void init() {
		dataSource = new DruidDataSource();
		dataSource.setDriverClassName(ConfigHandle.getString("db.driver"));
		dataSource.setUrl(ConfigHandle.getString("db.url"));
		dataSource.setUsername(ConfigHandle.getString("db.username"));
		dataSource.setPassword(ConfigHandle.getString("db.password"));
		// ==public
		// dataSource.setInitialSize(ConfigHandle.getInt("db.initialSize", 1));
		// dataSource.setMinIdle(ConfigHandle.getInt("db.minIdle", 1));
		// dataSource.setMaxActive(ConfigHandle.getInt("db.maxActive", 20));
		// dataSource.setMaxWait(ConfigHandle.getInt("db.maxWait", 60000));
		dataSource.setRemoveAbandoned(true);
		dataSource.setRemoveAbandonedTimeout(1800); 
		ConfigHandle.setProp(InstanceFactory.DS_FACTORY, getClass().getName());
	}

	@Override
	public void destroy() {
		dataSource.close();
	}

	@Override
	public DataSource getDataSource() {
		return dataSource;
	}

}
