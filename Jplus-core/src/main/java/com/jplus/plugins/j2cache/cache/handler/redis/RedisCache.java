package com.jplus.plugins.j2cache.cache.handler.redis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.BinaryJedisPubSub;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.util.SafeEncoder;

import com.jplus.framework.core.ConfigHandle;
import com.jplus.framework.util.serializer.SerializationUtils;
import com.jplus.plugins.j2cache.bean.Command;
import com.jplus.plugins.j2cache.cache.iface.Cache;
import com.jplus.plugins.j2cache.cache.listener.CacheListener;
import com.jplus.plugins.j2cache.fault.CacheException;

/**
 * Redis 缓存基于Hashs实现
 * 
 * @author wendal
 */
public class RedisCache extends BinaryJedisPubSub implements Cache {

	private final static Logger log = LoggerFactory.getLogger(RedisCache.class);

	// 记录region
	protected byte[] region2;
	protected String region;
	protected JedisPool pool;
	protected CacheListener listener;
	protected String channel = "j2cache_channel";

	public RedisCache(String region, JedisPool pool, CacheListener listener) {
		if (region == null || region.isEmpty())
			region = "_"; // 缺省region

		region = getRegionName(region);
		this.pool = pool;
		this.region = region;
		this.region2 = region.getBytes();
		this.listener = listener;
		new Thread(new Runnable() {
			@Override
			public void run() {
				try (Jedis jedis = RedisCacheProvider.getResource()) {
					jedis.subscribe(RedisCache.this, SafeEncoder.encode("j2cache_channel"));
				}
			}
		}).start();
	}

	/**
	 * 在region里增加一个可选的层级,作为命名空间,使结构更加清晰 同时满足小型应用,多个J2Cache共享一个redis database的场景
	 * 
	 * @param region
	 * @return
	 */
	private String getRegionName(String region) {
		String nameSpace = ConfigHandle.getString("redis.namespace", "");
		if (nameSpace != null && !nameSpace.isEmpty()) {
			region = nameSpace + ":" + region;
		}
		return region;
	}

	protected byte[] getKeyName(Object key) {
		if (key instanceof Number)
			return ("I:" + key).getBytes();
		else if (key instanceof String || key instanceof StringBuilder || key instanceof StringBuffer)
			return ("S:" + key).getBytes();
		return ("O:" + key).getBytes();
	}

	public Object get(Object key) throws CacheException {
		if (null == key)
			return null;
		Object obj = null;
		try (Jedis cache = pool.getResource()) {
			byte[] b = cache.hget(region2, getKeyName(key));
			if (b != null)
				obj = SerializationUtils.deserialize(b);
		} catch (Exception e) {
			log.error("Error occured when get data from redis2 cache", e);
			if (e instanceof IOException || e instanceof NullPointerException)
				evict(key);
		}
		return obj;
	}

	public void put(Object key, Object value) throws CacheException {
		if (key == null)
			return;
		if (value == null)
			evict(key);
		else {
			try (Jedis cache = pool.getResource()) {
				cache.hset(region2, getKeyName(key), SerializationUtils.serialize(value));
			} catch (Exception e) {
				throw new CacheException(e);
			}
		}
	}

	public void update(Object key, Object value) throws CacheException {
		put(key, value);
	}

	public void evict(Object key) throws CacheException {
		if (key == null)
			return;
		try (Jedis cache = pool.getResource()) {
			cache.hdel(region2, getKeyName(key));
		} catch (Exception e) {
			throw new CacheException(e);
		}
	}

	@SuppressWarnings("rawtypes")
	public void evict(List keys) throws CacheException {
		if (keys == null || keys.size() == 0)
			return;
		try (Jedis cache = pool.getResource()) {
			int size = keys.size();
			byte[][] okeys = new byte[size][];
			for (int i = 0; i < size; i++) {
				okeys[i] = getKeyName(keys.get(i));
			}
			cache.hdel(region2, okeys);
		} catch (Exception e) {
			throw new CacheException(e);
		}
	}

	public List<String> keys() throws CacheException {
		try (Jedis cache = pool.getResource()) {
			return new ArrayList<String>(cache.hkeys(region));
		} catch (Exception e) {
			throw new CacheException(e);
		}
	}

	public void clear() throws CacheException {
		try (Jedis cache = pool.getResource()) {
			cache.del(region2);
		} catch (Exception e) {
			throw new CacheException(e);
		}
	}

	public void destroy() throws CacheException {
		if (isSubscribed()) {
			this.unsubscribe();
		}
		this.clear();
	}

	// ==================================================================
	/**
	 * 接受广播。二级缓存接收到清除消息，所以现在清除一级缓存
	 */
	@Override
	public void onMessage(byte[] channel, byte[] message) {
		// 无效消息
		if (message != null && message.length <= 0) {
			log.warn("Message is empty.");
			return;
		}
		try {
			Command cmd = Command.parse(message);
			if (cmd == null || cmd.isLocalCommand())
				return;
			switch (cmd.getOperator()) {
			case Command.OPT_DELETE_KEY:
				listener.notifyElementExpired(LEVEL_1, cmd.getRegion(), cmd.getKey());
				break;
			case Command.OPT_CLEAR_KEY:
				listener.notifyElementExpired(LEVEL_1, cmd.getRegion(), null);
				break;
			default:
				log.warn("Unknown message type = " + cmd.getOperator());
			}
		} catch (Exception e) {
			log.error("Unable to handle received msg", e);
		}
	}

	/**
	 * 发送广播，清除缓存
	 */
	@Override
	public void sendEvictCmd(String region, Object key) throws CacheException {
		Command cmd = new Command(Command.OPT_DELETE_KEY, region, key);
		try (Jedis jedis = RedisCacheProvider.getResource()) {
			jedis.publish(SafeEncoder.encode(channel), cmd.toBuffers());
		} catch (Exception e) {
			log.error("Unable to delete cache,region=" + region + ",key=" + key, e);
		}
	}

}
