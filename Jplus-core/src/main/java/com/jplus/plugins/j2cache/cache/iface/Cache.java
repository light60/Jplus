package com.jplus.plugins.j2cache.cache.iface;

import java.util.List;

import com.jplus.plugins.j2cache.fault.CacheException;

/**
 * Implementors define a caching algorithm. All implementors <b>must</b> be
 * threadsafe.
 * 
 * @author liudong
 */
public interface Cache {
	public final static byte LEVEL_1 = 1;
	public final static byte LEVEL_2 = 2;

	/**
	 * Get an item from the cache, nontransactionally
	 */
	public Object get(Object key) throws CacheException;

	/**
	 * Add an item to the cache, nontransactionally, with failfast semantics
	 */
	public void put(Object key, Object value) throws CacheException;

	/**
	 * Add an item to the cache
	 */
	public void update(Object key, Object value) throws CacheException;

	@SuppressWarnings("rawtypes")
	public List keys() throws CacheException;

	/**
	 * Cache key Remove an item from the cache
	 */
	public void evict(Object key) throws CacheException;

	/**
	 * Batch remove cache objects
	 */
	@SuppressWarnings("rawtypes")
	public void evict(List keys) throws CacheException;

	/**
	 * Clear the cache
	 */
	public void clear() throws CacheException;

	/**
	 * Clean up
	 */
	public void destroy() throws CacheException;

	// ============================

	public void sendEvictCmd(String region, Object key) throws CacheException;

}
