package com.jplus.plugins.mybatis.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mybatis 日志记录<br>
 * 用于dao层接口上
 * 
 * @author yuanqy
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface JRemark {
	String remark();

	String key() default "";

	boolean show() default true;

	boolean showSql() default true;
}
