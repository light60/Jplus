package com.jplus.plugins.rpc.bean;

import com.jplus.framework.util.FormatUtil;
import com.jplus.framework.util.SecurityUtil;

/**
 * handle 的Key值<br>
 * 用于快速定位查找
 * 
 * @author Yuanqy
 *
 */
public class KeyNode {
	String serviceType;// 服务类型
	String serviceName;// 服务接口
	String version;// 服务版本

	public KeyNode(RpcRequest request) {
		this.serviceType = request.getServiceType().getName();
		this.serviceName = request.getIface().getName();
		this.version = request.getVersion();
	}

	public KeyNode(ZNode node) {
		this.serviceType = node.getServiceType().getName();
		this.serviceName = node.getIfaceName();
		this.version = node.getVersion();
	}

	public KeyNode(Service ser) {
		this.serviceType = ser.getRpcProvider().serviceType().getName();
		this.serviceName = ser.getIface().getName();
		this.version = ser.getRpcProvider().version();
	}
 
	@Override
	public String toString() {
		return FormatUtil.toJSONString(this);
	}

	public String MD5() {
		return SecurityUtil.MD5(toString());
	}

	public String getServiceType() {
		return serviceType;
	}

	public String getServiceName() {
		return serviceName;
	}

	public String getVersion() {
		return version;
	}
}