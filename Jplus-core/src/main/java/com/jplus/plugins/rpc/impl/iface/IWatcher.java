package com.jplus.plugins.rpc.impl.iface;

/**
 * 服务成功回调
 * 
 * @author Yuanqy
 *
 */
public interface IWatcher {
	public void process(boolean bo) throws Exception;
}