package com.jplus.plugins.rpc.bean;

import java.io.Serializable;

/**
 * RPC 返回参数
 * @author Yuanqy
 *
 */
public class RpcResponse implements Serializable {
	
	private static final long serialVersionUID = 9062588112032194955L;
	private String requestId;
	private Throwable error;
	private Object result;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public boolean isError() {
		return error == null ? false : true;
	}

	public Throwable getError() {
		return error;
	}

	public void setError(Throwable error) {
		this.error = error;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

}