package com.jplus.plugins.rpc.impl.handler.netty.coder;

import com.jplus.framework.util.serializer.SerializationUtils;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

@SuppressWarnings("rawtypes")
public class NettyEncoder extends MessageToByteEncoder {

	@Override
	public void encode(ChannelHandlerContext ctx, Object in, ByteBuf out) throws Exception {
		byte[] data = SerializationUtils.serialize(in);
		out.writeInt(data.length);
		out.writeBytes(data);
	}
}