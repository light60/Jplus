package test;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import com.jplus.plugins.activemq.ActiveMqTemplete;
import com.jplus.plugins.activemq.ActiveMqTemplete.IMsgHandler;

/**
 * 测试ActiveMQ
 * 
 * @author Yuanqy
 *
 */
public class TestMQ {
	public static void main(String[] args) {
		try {
			ActiveMqTemplete mq = new ActiveMqTemplete("tcp://192.168.1.11:61616", "TestMQ");
			for (int i = 0; i < 10; i++) {
				mq.sendMessage("我是一条测试消息" + i);
			}
			mq.startMsgListener(new IMsgHandler() {
				@Override
				public void receiveMsg(TextMessage textMsg) {
					try {
						System.err.println(textMsg.getText());
					} catch (JMSException e) {
						e.printStackTrace();
					}
				}
			}, 10);
			// =====================================
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
