package test;

import java.nio.ByteBuffer;

import org.junit.Test;

public class TestIO {
 
	@Test
	public void test() {
		String str = "com.xiaoluo.nio.MultipartTransfer";
		ByteBuffer buffer = ByteBuffer.allocate(50);
		for (int i = 0; i < str.length(); i++) {
			buffer.put(str.getBytes()[i]);
		}
		buffer.flip();
		int length = buffer.limit();
		byte[] buffer2 = new byte[100];

//		buffer.get(buffer2);
		buffer.get(buffer2, 0, length);
		System.out.println(length+":"+new String(buffer2).trim());
	}

}
