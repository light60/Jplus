package test;

/**
 * 异或与 运算符，制作权限验证<br>
 * 由于二进制的关系，只支持小于32种权限判断<br>
 * <br>
 * 可以理解为类似Linux的权限：rwx=421
 * 
 * @author Yuanqy
 *
 */
public class Test {
//	 按位与运算符（&）
//	 参加运算的两个数据，按二进制位进行“与”运算。
//	 运算规则：0&0=0; 0&1=0; 1&0=0; 1&1=1;
//	
//	 按位或运算符（|）
//	 参加运算的两个对象，按二进制位进行“或”运算。
//	 运算规则：0|0=0； 0|1=1； 1|0=1； 1|1=1；
//	
//	 异或运算符（^）
//	 参加运算的两个数据，按二进制位进行“异或”运算。
//	 运算规则：0^0=0； 0^1=1； 1^0=1； 1^1=0；
	
	public static void main(String[] args) {
		int db = 0;// 初始化空权限值
		for (int i = 0; i <= 5; i++) {
			db = addVerify(db, 1 << i);// 添加0~5的权限
		}
		System.out.println("权限值：" + db);
		db = cancelVerify(db, 1 << 3);// 删除权限3
		db = addVerify(db, 1 << 7);// 添加权限7
		for (int i = 0; i <= 7; i++) {
			System.out.println(i + "==" + queryVerify(db, 1 << i));// 判断0~7的权限
		}
		// =================================
		db = cancelVerify(db, 1 << 0);// 删除权限0
		System.out.println("0也是一种权限哦:" + queryVerify(db, 1 << 0));
	}

	/**
	 * 添加 验证模块
	 * 
	 * @param verify
	 * @return
	 */
	public static int addVerify(int... verify) {
		int dbStore = 0;
		for (int i = 0; i < verify.length; i++) {
			dbStore |= verify[i];
		}
		return dbStore;
	}

	/**
	 * 查询当前 verify 是否开启验证
	 * 
	 * @param dbStore
	 * @param verify
	 * @return
	 */
	public static boolean queryVerify(int dbStore, int verify) {

		if ((dbStore & verify) == verify) {
			return true;
		}
		return false;
	}

	/**
	 * 取消指定 verify 验证
	 * 
	 * @param dbStore
	 * @param verify
	 */
	public static int cancelVerify(int dbStore, int verify) {
		return dbStore & (~verify);
	}
}
