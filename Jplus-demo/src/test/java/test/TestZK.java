package test;

import java.util.List;
import java.util.Scanner;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;

import com.jplus.framework.util.FormatUtil;
import com.jplus.plugins.rpc.core.ZkFactory;

public class TestZK {

	@org.junit.Test
	public void test() {
		try {
			int SESSION_TIMEOUT = 10000;
			String CONNECTION_STRING = "192.168.1.23:2081";
			// =============================================
			ZkFactory.createConnection(CONNECTION_STRING, SESSION_TIMEOUT);
			boolean bo = true;
			while (bo) {
				forEach("/", ZkFactory.getZK(), -1);
				System.out.print("选择操作:\n1.刷新\t2.删除\t9.退出\n");
				@SuppressWarnings("resource")
				int ind = FormatUtil.toInt(new Scanner(System.in).next());
				switch (ind) {
				case 2:
					System.out.println("请输入节点名称：\n");
					String temp = new Scanner(System.in).next();
					ZkFactory.deleteNode(temp);
					break;
				case 9:
					bo = false;
				default:
					break;
				}
			}
			ZkFactory.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void forEach(String path, ZooKeeper zk, int dep) throws KeeperException, InterruptedException {
		List<String> list = zk.getChildren(path, false);
		++dep;
		if (list.size() > 0)
			for (String str : list) {
				String temp = FormatUtil.formatPath(path + "/" + str);
				byte[] data = zk.getData(temp, false, null);
				System.err.println(tabkey(dep) + temp + "\t[value=" + new String(data == null ? new byte[] {} : data) + "]");
				forEach(temp, zk, dep);
			}
	}

	private String tabkey(int dep) {
		String t = "";
		for (int i = 0; i < dep; i++) {
			t += "  ";
		}
		return t;
	}
}