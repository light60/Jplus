package test.junit;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.demo.remote.dto.Dto;
import com.demo.remote.iface.ITestServer;
import com.jplus.framework.junit.JplusJunit4Runner;
import com.jplus.framework.junit.annotation.ContextConfiguration;
import com.jplus.plugins.rpc.annotation.RpcConsumer;
import com.jplus.plugins.rpc.impl.handler.ServiceType;

@RunWith(JplusJunit4Runner.class)
@ContextConfiguration(locations = "classpath:app.properties")
public class _Test_RPC {

	private @RpcConsumer(serviceType = ServiceType.Scoket) ITestServer rpcClient1;
	private @RpcConsumer(serviceType = ServiceType.Netty) ITestServer rpcClient2;
	private @RpcConsumer(serviceType = ServiceType.Http) ITestServer rpcClient3;
	
	@Test
	public void test() {
		try {
			for (int i = 0; i < 10; i++) {
				Dto dto = new Dto("RPC", i);
				
				System.err.println(rpcClient1.getMsg(dto));
				System.err.println(rpcClient2.getMsg(dto));
				System.err.println(rpcClient3.getMsg(dto));
				
				Thread.sleep(10 * 1000);
				System.out.println("\n\n\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
