package test.junit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo.remote.dto.Dto;
import com.demo.web.service.impl.TestServerImpl;
import com.jplus.framework.ioc.annotation.Autowired;
import com.jplus.framework.junit.JplusJunit4Runner;
import com.jplus.framework.junit.annotation.ContextConfiguration;

@RunWith(JplusJunit4Runner.class)
@ContextConfiguration(locations = "classpath:app.properties")
public class _TestJ2CacheAnno {
	private Logger log = LoggerFactory.getLogger(getClass());
	private @Autowired TestServerImpl server;

	@Test
	public void test() {
		for (int i = 0; i < 10; i++) {
			log.info("缓存测试：" + server.getMsg(new Dto("缓存", i)));
		}
	}
}
