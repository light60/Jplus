package test;

import java.lang.reflect.Method;
import java.util.Arrays;

import com.jplus.framework.aop.AspectProxy;
import com.jplus.framework.aop.proxy.Proxy;
import com.jplus.framework.aop.proxy.ProxyManager;

public class TestAopCglib {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException {
		Proxy pro = new TestProxy();
		TestObj to = ProxyManager.createProxy(TestObj.class, Arrays.asList(pro),null);
		to.method2();
	}

	public static class TestObj {
		public void method1() {
			System.out.println("This is method1");
		}

		public void method2() {
			method1();
			System.out.println("This is method2");
		}
	}

	public static class TestProxy extends AspectProxy {
		@Override
		public void before(Class<?> cls, Method method, Object[] params) throws Throwable {
			System.out.println("====Aspect before：" + method.getName());
		}

		@Override
		public void after(Class<?> cls, Method method, Object[] params, Object result) throws Throwable {
			System.out.println("====Aspect after：" + method.getName());
		}
	}
}
