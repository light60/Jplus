package test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * 测试Thread ，Runnable，Callable <br>
 * FutureTask 实现了RunnableFuture<V>，是个综合体。<br>
 * 以上所有多线程操作都会出现线程安全问题，需要特别处理。
 * 
 * @author Yuanqy
 *
 */
public class TestThread {

	public static void main(String[] args) {
		try {
			MyThread1 t1 = new MyThread1();// Thread
			MyThread2 t2 = new MyThread2();// Runnable
			MyThread3 t3 = new MyThread3();// Callable ==>Future
			MyThread3 tft = new MyThread3();// Callable ==>FutureTask

			ExecutorService es = Executors.newFixedThreadPool(100);
			for (int i = 0; i < 2; i++) {
				es.execute(t1);
				es.execute(t2);

				Future f1 = es.submit(t3);
				System.err.println("Future:" + f1.get());// get是阻塞的

				FutureTask ft = new FutureTask<Integer>(tft);// 使得Callable能用Runnable驱动
				es.execute(ft);
				System.err.println("FutureTask:" + ft.get());// get是阻塞的
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static class MyThread1 extends Thread {
		private int ticket = 0;

		public void run() {
			for (int i = 0; i < 10; i++) {
				System.out.println("[Thread]" + Thread.currentThread().getName() + ":" + (++ticket));
			}
		}
	}

	static class MyThread2 implements Runnable {
		private int ticket = 0;

		public void run() {
			for (int i = 0; i < 10; i++) {
				System.out.println("[Runnable]" + Thread.currentThread().getName() + ":" + (++ticket));
			}
		}
	}

	static class MyThread3 implements Callable<Integer> {
		private int ticket = 0;

		@Override
		public Integer call() throws Exception {
			for (int i = 0; i < 10; i++) {
				System.out.println("[Callable]" + Thread.currentThread().getName() + ":" + (++ticket));
			}
			return new Integer(ticket);// 有返回值
		}
	}
}
