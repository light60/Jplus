package test;

public class Test2222 {
	public static void main(String[] args) {
		int i = (int) Math.pow(2, 32);
		System.out.println(i);
		System.err.println(byte2int(int2byte(i)));
		
		
		
		int a=5,b=3;
		a=a^b;
		b=a^b;
		a=a^b;
		System.out.println(a+"="+b);
	}

	public static byte[] int2byte(int res) {
		byte[] targets = new byte[4];
		targets[0] = (byte) (res & 0xff);// 最低位
		targets[1] = (byte) ((res >> 8) & 0xff);// 次低位
		targets[2] = (byte) ((res >> 16) & 0xff);// 次高位
		targets[3] = (byte) (res >>> 24);// 最高位,无符号右移。
		return targets;
	}

	public static int byte2int(byte[] res) {
		// 一个byte数据左移24位变成0x??000000，再右移8位变成0x00??0000
		// | 表示安位或
		int targets = (res[0] & 0xff) | ((res[1] << 8) & 0xff00) | ((res[2] << 24) >>> 8) | (res[3] << 24);
		return targets;
	}
	
	
}
