package com.demo.web.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.demo.web.entity.Blog;
import com.jplus.plugins.mybatis.annotation.JRemark;
import com.jplus.plugins.mybatis.plugin.JPage;

public interface IBlogDao {

	@JRemark(remark = "分页查询")
	List<Blog> listBlog(JPage page);

	void addBlog(Blog blog);

	void delBlog(@Param("id")int id);

	void editBlog(Blog blog);

	@JRemark(remark = "根据ID查询博客")
	Blog getBlogById(@Param("id")int id);
}
