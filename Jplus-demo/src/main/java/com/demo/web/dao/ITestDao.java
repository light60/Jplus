package com.demo.web.dao;

import java.util.List;
import java.util.Map;

import com.jplus.plugins.mybatis.annotation.JRemark;

/**
 * 数据库操作dao层<br>
 * 使用Mybatis插件实现
 * 
 * @author Yuanqy
 *
 */
public interface ITestDao {

	@JRemark(remark = "获取数据库所有表", show = false)
	List<Map<String, Object>> listAllTable();

	@JRemark(remark = "获取数据库当前时间")
	Map<String, Object> getNow();

}
