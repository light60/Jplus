package com.demo.web.action;

import com.demo.web.entity.Blog;
import com.demo.web.service.BlogServer;
import com.jplus.framework.ioc.annotation.Autowired;
import com.jplus.framework.mvc.annotation.Controller;
import com.jplus.framework.mvc.annotation.Request;
import com.jplus.framework.mvc.bean.Result;
import com.jplus.framework.mvc.bean.View;
import com.jplus.plugins.mybatis.plugin.JPage;

/**
 * MVC 测试
 * 
 * @author Yuanqy
 *
 */
@Controller
// 标示当前类为Action，必须
@Request.All("/mvc")
// action 前缀，非必须
public class WebAction {

	// 自动注入server
	private @Autowired BlogServer server;

	/**
	 * [get]获取博客列表，第{page}页
	 */
	@Request.Get("/blog/list/{page}")
	public View home(int page) {
		JPage jpage = new JPage();
		jpage.setPage(page);
		server.listBlog(jpage);
		return new View("/mvc/blog/list").add("jpage", jpage);
	}

	/**
	 * [get]获取博客，id为{id}
	 */
	@Request.Get("/blog/{id}")
	public Result getBlog(int id) {
		Result res = new Result();
		res.DATA(server.getBlog(id));
		return res;
	}

	/**
	 * [post]新增博客
	 */
	@Request.Post("/blog")
	public Result addBlog(Blog blog) {
		try {
//			Blog blog = WebUtil.getRequestParamBean(Blog.class);
			server.addBlog(blog);
			return new Result().OK();
		} catch (Exception e) {
			e.printStackTrace();
			return new Result().FAIL();
		}
	}

	/**
	 * [put]修改博客
	 */
	@Request.Put("/blog")
	public Result editBlog(Blog blog) {
		try {
			server.editBlog(blog);
			return new Result().OK();
		} catch (Exception e) {
			e.printStackTrace();
			return new Result().FAIL();
		}
	}

	/**
	 * [Delete]删除博客，id为{id}
	 */
	@Request.Delete("/blog/{id}")
	public Result delBlog(int id) {
		Result res = new Result();
		server.delBlog(id);
		return res;
	}

}
