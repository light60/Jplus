package com.demo.web.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.demo.remote.dto.Dto;
import com.demo.remote.iface.ITestServer;
import com.demo.web.service.impl.TestServer2Impl;
import com.jplus.framework.db.JdbcTemplate;
import com.jplus.framework.ioc.annotation.Autowired;
import com.jplus.framework.ioc.annotation.Value;
import com.jplus.framework.mvc.annotation.Controller;
import com.jplus.framework.mvc.annotation.Param;
import com.jplus.framework.mvc.annotation.Request;
import com.jplus.framework.mvc.bean.Result;
import com.jplus.plugins.druid.DruidPlugin;
import com.jplus.plugins.rpc.annotation.RpcConsumer;

/**
 * 测试Action
 * 
 * @author Yuanqy
 *
 */
@Controller
public class TestAction {

	private @Autowired ITestServer server1;// 通过接口注入
	private @Autowired("Server2") ITestServer server2;// 通过指定接口实现注入
	private @Autowired TestServer2Impl server3;// 通过指定对象注入
	private @Value("rpc.registry.address") String registryAddress;// 注入配置文件
	private @Autowired DruidPlugin druid;// 插件注入

	private @RpcConsumer ITestServer server;// RPC客户端代理注入

	@Request.Get("/ioc")
	public Result ioc() {
		Dto dto = new Dto("老袁", 18);
		Result res = new Result();
		System.out.println("==通过接口注入");
		System.out.println(server1.getMsg(dto));
		System.out.println("==通过指定接口实现注入");
		System.out.println(server2.getMsg(dto));
		System.out.println("==通过指定对象注入");
		System.out.println(server3.getMsg(dto));
		res.DATA(server2.getMsg(dto));
		return res;
	}

	@Request.Get("/jdbc")
	public Result jdbc() throws Exception {
		Result res = new Result();
		JdbcTemplate jdbc = druid.getJdbcTemplate();
		List<Map<String, Object>> list = jdbc.queryList("SELECT table_name FROM information_schema.tables");
		res.DATA(list);
		return res;
	}

	@Request.Get("/rpc")
	public Result rpc() {
		Result res = new Result();
		res.DATA(server.getMsg(new Dto("老袁", 18)));
		return res;
	}

	/**
	 * 入参测试<br>
	 * 入参是无序的。
	 * 
	 * @param req
	 *            获取http对象，可以支持4种类型
	 * @param xb
	 *            默认取值restful下标入参
	 * @param xx
	 *            按名称取值，可取restful、request入参
	 */
	@Request.Get("/param/{xx}")
	public Result param(HttpServletRequest req, String xb, @Param("xx") String xx) {
		System.err.println("req:" + req.getParameter("xx"));
		System.err.println("下标:" + xb);
		System.err.println("注解:" + xx);
		/**
		 * 结论：xb==xx的，如过request和restful均有同名入参，优先以reqeust为准。
		 * 
		 * restful下标取值说明： 方法入参支持类型[9种],4种http+5种基本数据类型，除去4种http,
		 * 剩下无@Param注解的基本数据类型入参，按restful中{xx}占位顺序依次取值，注入。
		 * 
		 */
		return new Result();
	}
}
