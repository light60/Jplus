package com.demo.web.service.impl;

import com.demo.remote.dto.Dto;
import com.demo.remote.iface.ITestServer;
import com.jplus.framework.ioc.annotation.Value;
import com.jplus.framework.mvc.annotation.Service;
import com.jplus.framework.util.FormatUtil;

/**
 * 指定服务名，创建实例
 * 
 * @author Yuanqy
 *
 */
@Service("Server2")
public class TestServer2Impl implements ITestServer {
	@Value("testMsg")
	private String msg;

	@Override
	public String getMsg(Dto dto) {
		return "[Server2]:" + FormatUtil.toJSONString(dto) + msg;
	}

}
