package com.demo.web.service.impl;

import com.demo.remote.dto.Dto;
import com.demo.remote.iface.ITestServer;
import com.demo.web.dao.ITestDao;
import com.jplus.framework.db.annotation.Transaction;
import com.jplus.framework.ioc.annotation.Autowired;
import com.jplus.framework.ioc.annotation.Value;
import com.jplus.framework.mvc.annotation.Service;
import com.jplus.framework.util.FormatUtil;
import com.jplus.plugins.j2cache.annotation.Cacheable;

/**
 * 开启事务的基本服务
 * 
 * @author Yuanqy
 *
 */
@Service
public class TestServerImpl implements ITestServer {

	private @Value("testMsg") String msg;

	private @Autowired ITestDao dao;

	@Override
	@Cacheable // 开启缓存
	@Transaction // 开启事务
	public String getMsg(Dto dto) {
		return FormatUtil.toJSONString(dto) + msg + dao.getNow();
	}
}
