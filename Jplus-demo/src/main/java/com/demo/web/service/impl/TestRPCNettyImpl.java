package com.demo.web.service.impl;

import com.demo.remote.dto.Dto;
import com.demo.remote.iface.ITestServer;
import com.demo.web.dao.ITestDao;
import com.jplus.framework.db.annotation.Transaction;
import com.jplus.framework.ioc.annotation.Autowired;
import com.jplus.framework.ioc.annotation.Value;
import com.jplus.framework.util.FormatUtil;
import com.jplus.plugins.rpc.annotation.RpcProvider;

/**
 * 开启RPC的服务提供者[默认Netty]
 * 
 * @author Yuanqy
 *
 */
@RpcProvider(iface = ITestServer.class)
public class TestRPCNettyImpl implements ITestServer {
	private @Value("testMsg") String msg;

	private @Autowired ITestDao dao;

	@Override
	@Transaction
	// 开启事务
	public String getMsg(Dto dto) {
		return FormatUtil.toJSONString(dto) + msg + dao.getNow() + "[This is Netty RPC]";
	}

}
