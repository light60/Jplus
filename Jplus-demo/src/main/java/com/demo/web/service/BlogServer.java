package com.demo.web.service;

import com.demo.web.dao.IBlogDao;
import com.demo.web.entity.Blog;
import com.jplus.framework.ioc.annotation.Autowired;
import com.jplus.framework.mvc.annotation.Service;
import com.jplus.plugins.mybatis.plugin.JPage;

@Service
public class BlogServer {

	private @Autowired IBlogDao dao;

	public JPage listBlog(JPage jpage) {
		dao.listBlog(jpage);
		return jpage;
	}

	public Blog getBlog(int id) {
		return dao.getBlogById(id);
	}

	public void addBlog(Blog blog) {
		dao.addBlog(blog);
	}

	public void editBlog(Blog blog) {
		dao.editBlog(blog);
	}

	public void delBlog(int id) {
		dao.delBlog(id);
	}

}
