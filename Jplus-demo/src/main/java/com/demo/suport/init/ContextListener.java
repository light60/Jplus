package com.demo.suport.init;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.framework.AppConstant;

/**	
 * 在加载前启动[非必须]<br>
 * 在这里可以修改一些配置信息
 * J2EE加载顺序：context-param -> listener -> filter -> servlet
 * 
 * @author Yuanqy
 */
@WebListener
public class ContextListener implements ServletContextListener {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public void contextInitialized(ServletContextEvent sce) {
		logger.info("########## 我在项目启动之前执行 ##########");
		// 加载配置文件,多个文件以;分割
		// AppConstant.CONFIG.refresh("file:/app.properties");//系统根目录
		// AppConstant.CONFIG.refresh("app.properties;app2.properties");//项目根目录
		// AppConstant.CONFIG.refresh("/app.properties");//项目WEB-INF下
		AppConstant.CONFIG.refresh("classpath:app.properties");// 项目ClassPath下【默认】
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		logger.info("##### 我在项目注销之前执行##########");
	}

}
