package com.demo.suport.init;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.framework.bean.BeanHandle;
import com.jplus.framework.bean.annotation.Component;
import com.jplus.framework.ioc.annotation.Value;
import com.jplus.plugins.mybatis.MybatisPlugin;
import com.jplus.plugins.mybatis.plugin.MybatisInterceptor_Page;
import com.jplus.plugins.mybatis.plugin.MybatisInterceptor_SqlLog;

/**
 * 普通组件类，添加[initMethod、destroyMethod]
 * 
 * @author Yuanqy
 *
 */
@Component(initMethod = "init", destroyMethod = "destroy")
public class ContextComponent {

	final Logger logger = LoggerFactory.getLogger(getClass());

	private @Value("db.type") String dbType;

	public void init() {
		logger.info("==========项目初始化完毕===========");
		// ==为mybatis 添加拦截器
		MybatisPlugin mybatis = BeanHandle.getBean(MybatisPlugin.class);
		mybatis.addPluginElement(new MybatisInterceptor_Page(dbType));
		mybatis.addPluginElement(new MybatisInterceptor_SqlLog());

	}

	public void destroy() {
		logger.info("==========项目注销===========");
	}
}