package com.demo.suport.init;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.framework.aop.AspectProxy;
import com.jplus.framework.aop.annotation.Aspect;
import com.jplus.framework.mvc.DataContext;
import com.jplus.framework.mvc.DispatcherServlet;
import com.jplus.framework.mvc.annotation.Controller;
import com.jplus.framework.util.UUIDUtil;

/**
 * AOP拦截器演示<br/>
 * 测试拦截所有的Action
 * 
 * @author Yuanqy
 */
@Aspect(pkg = "com.demo.web.action", annotation = Controller.class)
public class HandlerInterceptor extends AspectProxy {
	private Logger logger = LoggerFactory.getLogger(getClass());
	// 单例非线程安全
	ThreadLocal<Long> curTimes = new ThreadLocal<Long>();

	@Override
	public void before(Class<?> cls, Method method, Object[] params) throws Throwable {
		curTimes.set(System.currentTimeMillis());
		logger.info("===Is new RequestURI：" + DataContext.getRequest().getRequestURI());
		logger.info("===Action:{}.{}", cls.getName(), method.getName());
		DataContext.getRequest().setAttribute("v", UUIDUtil.getUUID(5, 10));
	}

	@Override
	public void after(Class<?> cls, Method method, Object[] params, Object result) throws Throwable {
		logger.info("===Action is complete,time：{}ms \n", System.currentTimeMillis() - curTimes.get() );
	}

}
