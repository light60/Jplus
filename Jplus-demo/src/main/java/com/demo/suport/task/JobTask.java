package com.demo.suport.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo.remote.dto.Dto;
import com.demo.web.service.impl.TestServerImpl;
import com.jplus.framework.bean.BeanHandle;
import com.jplus.plugins.quartz.annotation.JTask;

/**
 * 定时器任务Demo<br>
 * 依赖插件：com.jplus.plugins.quartz.QuartzPlugin
 * 
 * @author Yuanqy
 *
 */
@JTask(cron = "0 0/10 * * * ?")
//@JTask(cron = "${task.cron}") //支持读取配置信息
public class JobTask implements Job {
	Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("==定时任务执行=========");
		TestServerImpl server = BeanHandle.getBean(TestServerImpl.class);
		logger.info("==执行服务：" + server.getMsg(new Dto("作业", 0)));
	}

}
