package com.demo.remote.dto;

import java.io.Serializable;

public class Dto implements Serializable {
	private static final long serialVersionUID = -7666900376086617078L;
	
	String name;
	int age;

	public Dto(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public Dto() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
