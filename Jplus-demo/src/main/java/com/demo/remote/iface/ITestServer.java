package com.demo.remote.iface;

import com.demo.remote.dto.Dto;

/**
 * 服务接口[作接口注入测试/RPC测试]
 * 
 * @author Yuanqy
 *
 */
public interface ITestServer {

	String getMsg(Dto dto);
}
