<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="${ctx}/resource/img/favicon.ico?v=${v}" type="image/x-icon" />
<meta charset="UTF-8">
<title>Jplus-Demo</title>
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="app-mobile-web-app-capable" content="yes">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${ctx}/resource/css/base.css?v=${v}" />
<link rel="stylesheet" type="text/css" href="${ctx}/resource/css/grid.css?v=${v}" />
<link rel="stylesheet" type="text/css" href="${ctx}/resource/css/basePC.css?v=${v}" />
<script type="text/javascript" src="${ctx}/resource/js/jquery.min.js?v=${v}"></script>
<script type="text/javascript" src="${ctx}/resource/js/common.js?v=${v}"></script>
<style type="text/css">
.PW {margin: auto;	width: 100%; max-width: 1024px;position: relative;}
.panel {	background: white;}
.menu ul {	list-style: none;	color: white;}
.menu ul li {float: left;padding: 0 15px;cursor: pointer}
.menu ul li:hover {	background-color: rgba(0, 0, 0, 0.1);}
li a {color: white;}
table tr:hover td {background: #F0FAFF;}
.alertMsg{position:fixed;top:5px;left:50%;margin-left:-200px;padding:10px;width:400px;border:2px solid #1FAA39;border-radius:5px;box-shadow:0 0 5px #B7B7B7;text-align:center;line-height:20px;z-index: 100;}
</style>
</head>
<body class="lh30">
	<!-- TopMenu1 -->
	<div class="panel" style="background: #2F8BCD;border-radius: 0px;">
		<div class="PW menu lh40  ">
			<ul class="fl fb">
				<li><a href="${ctx}/">Jplus-MVC DEMO</a></li>
			</ul>
		</div>
	</div>
	<!--  -->
	<div class="panel">
		<div class="PW ">
			<div class="">
				<div class="panel-header mt10">
					<label class="title bb2">文章列表</label>
				</div>
				<div class="panel-content  ">
					<div class="p10 clear ">
						<button class="pl20 pr20 _blue" onclick="addBlog()">新增</button>
						<div class="fr tr" style="z-index: -1; line-height: 20px;">
							共${jpage.count}条记录, 当前${jpage.page}/ ${jpage.allPage} 页
							<button type="button" onclick="doPage(-1)">上一页</button>
							<button type="button" onclick="doPage(1)">下一页</button>
						</div>
					</div>
					<div class="p10  clear">
						<table class="table  ">
							<c:forEach var="temp" items="${jpage.data}">
								<tr>
									<td>${temp.id}</td>
									<td width="50%">${temp.title}</td>
									<td>${temp.createTime}</td>
									<td class="tc">
										<button class="pl10 pr10 _blue" onclick="getBlog(${temp.id})">查看</button>
										<button class="pl10 pr10 _yellow" onclick="editBlog(${temp.id})">修改</button>
										<button class="pl10 pr10 _red" onclick="delBlog(${temp.id})">删除</button>
									</td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 新增/修改  -->
	<div class="hide BlogInfo">
		<input type="hidden" name="id"/>
		<div class="row clear">
			<span class="g-1-6 tc">标题：</span> <span class="g-5-6"><input type="text" name="title" /></span>
		</div>
		<div class="row  mt5">
			<span class="g-1-6 tc">内容：</span> <span class="g-5-6"><textarea rows="6" name='content'></textarea> </span>
		</div>
	</div>
	<!-- 查询 -->
	<div class="SeeInfo hide">
		<div class="row tc p30" style="border-bottom: 2px dashed white;;background: #5DA8E8;color: white;margin: -10px;">
			<h1 class="title"></h1>
			<h5 class="createTime"></h5>
		</div>
		<div class="row p10">
			<div >
				<pre class="content"></pre>
			</div>
		</div>
	</div>
	
	<!-- 弹出框 -->
	<div id="Dialog" class="hide">
		<div class="_model">&nbsp;</div>
		<div class="panel PW oh">
			<button class="btnCannel cannelBtn _small fb " >×</button>
			<div class="panel-header p5 hide">
				<label class="title "></label>
			</div>
			<div class="panel-content clear ">
				<form class="m10"></form>
			</div>
			<div class="panel-footer tr">
				<button class="pl30 pr30 _def m10 btnCannel "></button>
				<button class="pl30 pr30 _blue m10 btnOk "></button>
			</div>
		</div>
	</div>
	<!-- 提示框 -->
	<div class="alertMsg hide">&nbsp;</div>
	<script type="text/javascript">
		function doPage(no){
			var p = no + parseInt("${jpage.page}"); 
			p = p > 0 ? p : 1;
			location.href="${ctx}/mvc/blog/list/"+p;
		}
		function getBlog(id){
			sendRequest("get","${ctx}/mvc/blog/"+id,{}, function(data){
				if(data.code==1){
					showAlert({
							title:'',					
							msg : '.SeeInfo',
							msgType : 'jquery',// jquery
							btnOkShow : false,
							btnCannelShow:false
					});
					$("#Dialog .title").text(data.obj.title);
					$("#Dialog .content").text(data.obj.content);
					$("#Dialog .createTime").text(data.obj.createTime);
				}else{
					showMsg("博客查询失败~","error");
				}
			});
		}
		
		function addBlog(){
			var option = {
					title : '新增博客',
					msg : '.BlogInfo',
					msgType : 'jquery',// jquery
					btnCannelVal : '取消', 
					btnOkVal : '确认新增',  
					btnOkFunc:function(){
						sendRequest("post","${ctx}/mvc/blog", $("#Dialog form").serialize(), function(data){
							if(data.code==1){
								closeAlert();
								location.reload();
							}
						});
					}
				};
			showAlert(option);
		}
		function editBlog(id){
			sendRequest("get","${ctx}/mvc/blog/"+id,{}, function(data){
				if(data.code==1){
					showAlert({
							title : '修改博客', 
							msg : '.BlogInfo',
							msgType : 'jquery',// jquery
							btnCannelVal : '取消',  
							btnOkVal : '确认修改',   
							btnOkFunc:function(){
								sendRequest("put","${ctx}/mvc/blog", $("#Dialog form").serialize(), function(data){
									if(data.code==1){
										closeAlert(); 
										location.reload();
									}
								});
							}
					});
					$("#Dialog input[name=id]").val(data.obj.id);
					$("#Dialog input[name=title]").val(data.obj.title);
					$("#Dialog textarea[name=content]").text(data.obj.content);
				}else{
					showMsg("博客获取失败~","error"); 
				}
			});
		}
		function delBlog(id){
			showAlert({
				width:300,
				title : '删除博客',  
				msg : '确认删除当前博客？',
				btnCannelVal : '取消',  
				btnOkVal : '确认删除',    
				btnOkFunc:function(){
					sendRequest("delete","${ctx}/mvc/blog/"+id,{}, function(data){
						if(data.code==1){ 
							closeAlert(); 
							location.reload();
						}
					});
				}});
		}
		
	</script>
</body>
</html>