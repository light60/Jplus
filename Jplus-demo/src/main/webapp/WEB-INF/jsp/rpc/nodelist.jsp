<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="${ctx}/resource/img/favicon.ico?v=${v}" type="image/x-icon" />
<meta charset="UTF-8">
<title>Jplus-Demo</title>
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="app-mobile-web-app-capable" content="yes">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${ctx}/resource/css/base.css?v=${v}" />
<link rel="stylesheet" type="text/css" href="${ctx}/resource/css/grid.css?v=${v}" />
<link rel="stylesheet" type="text/css" href="${ctx}/resource/css/basePC.css?v=${v}" />
<script type="text/javascript" src="${ctx}/resource/js/jquery.min.js?v=${v}"></script>
<script type="text/javascript" src="${ctx}/resource/js/common.js?v=${v}"></script>
<style type="text/css">
.PW {margin: auto;	width: 100%; max-width: 1024px;position: relative;}
.panel {background: white;}
.menu ul {list-style: none;	color: white;}
.menu ul li {float: left;padding: 0 15px;cursor: pointer}
.menu .action,.menu ul li:hover {	background-color: rgba(0, 0, 0, 0.1);}


li a {color: white;}
.bb1{border: 1px solid #AAAAAA; margin: -1px;box-shadow: 0px 0px 5px #c8c8c8;}

.jtree p:hover{cursor: pointer;}
.jtree li:hover{background-color: rgba(0,0,0,0.1);}
.jtree>ul{margin-left:0px !important;}
</style>
</head>
<body class="lh30">
	<!-- TopMenu1 -->
	<div class="panel" style="background: #2F8BCD;border-radius: 0px;">
		<div class="PW menu lh40  ">
			<ul class="fl fb">
				<li><a href="${ctx}/">Jplus-RPC monitor</a></li>
			</ul>
			<ul class="fr">
				<li class="action"><a href="${ctx}/">节点列表</a></li>
				<li><a href="${ctx}/">提供者</a></li>
				<li><a href="${ctx}/">消费者</a></li>
			</ul>
		</div>
	</div>
	<!--  -->
	<div class="panel">
		<div class="PW ">
			<div class="">
				<div class="panel-header mt10">
					<label class="title bb2">节点列表</label>
				</div>
				<div class="panel-content clear pt10">
					<div class="bb1 jtree" style="overflow: auto;max-height: 400px;">
						 &nbsp;
					</div>
					<div class="clear bb1">
						<div class="m10 clear">
							<div class="row">
								<div class="g-3-24  tc">节点名：</div>
								<div class="g-21-24">
									<textarea rows="5" cols="" id='zName'></textarea>
								</div>
							</div>
							<div class="row">
								<div class="g-3-24  tc">节点值：</div>
								<div class="g-21-24">
									<textarea rows="3" cols="" id='zValue'></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			sendRequest("get","${ctx}/rpc/ajaxZkTree/0",{}, function(data){
				if(data.code==1){
					var html=eachData(data.obj);
					console.log(html);
					$(".jtree").html(html);
					$(".jtree p").click(function(){
						var $li=$(this).parent();
						$("#zName").text(decodeURIComponent($(this).text()));
						$("#zValue").val($li.attr("data"));
					});
				}else{
					showMsg("博客查询失败~","error");
				}
			});
		});
		
		function eachData(data){
			var dom='<ul class="ml20">';
			$(data).each(function(i,val) {
				dom+='<li data="'+val.data+'" class="pl10"><p>'+val.key+'</p>'+eachData(val.childs)+'</li>'
			});
			dom+='</ul>'
			return dom;
		}
	</script>
</body>
</html>