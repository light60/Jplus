
// =======模态弹出框=======================
function closeAlert(){
	$("#Dialog").fadeOut("fast");
}
function showAlert(options) {
	var $dialog=$("#Dialog>.panel");
	var $dialog_h=$dialog.find(".panel-header").hide();
	var $dialog_c=$dialog.find(".panel-content>form");
	var $dialog_f=$dialog.find(".panel-footer");
	
	var dfoption = {
		width : 600,// 弹出框宽度
		height : 0,// 弹出框高度，
		title : '',
		msg : 'This is msg',
		msgType : '',// jquery
		btnCannelVal : 'Cannel',
		btnCannelShow : true,
		btnCannelFunc:closeAlert,
		btnOkVal : 'Let\'s do it',
		btnOkShow : true,
		btnOkFunc:function(){alert("OK");}
	};
	options = $.extend(dfoption, options);
	if (options.width > $(window).width())
		options.width = $(window).width() - 10;
	$dialog_c.html(options.msg);
	var _top = "20%";
	if (options.height > 10) {
		var th = ($(window).height() - options.height) / 2;
		_top = th > 0 ? th + "px" : "0px";
	}
	$dialog.css({
		"left" : "50%",
		"top" : _top,
		"margin-left" : "-" + (options.width / 2 + 1) + "px",
		"width" : options.width + "px"
	});
	if (options.msgType == 'jquery')
		$dialog_c.html($(options.msg).html());
	$dialog_f.find(".btnOk").hide();
	$dialog_f.find(".btnCannel").hide();
	if(!(options.title=='' || options.title==undefined || options.title==null))
		$dialog_h.show().find(".title").text(options.title);
	if (options.btnOkShow){
		$dialog_f.find(".btnOk").text(options.btnOkVal).show();
		$dialog_f.find(".btnOk").unbind("click").bind("click",options.btnOkFunc);
	}
	if (options.btnCannelShow)
		$dialog_f.find(".btnCannel").text(options.btnCannelVal).show();
	$dialog.find(".btnCannel").unbind("click").bind("click",options.btnCannelFunc);
	//==位移== 
	var _move = false;// 移动标记
	var _x, _y;// 鼠标离控件左上角的相对位置
	$dialog_h.css("cursor","move");
	$dialog_h.unbind("mousedown").bind("mousedown",function(e) {
		_move = true;
		_x = e.pageX - parseInt($dialog.css("left"));
		_y = e.pageY - parseInt($dialog.css("top"));
		$dialog.fadeTo(20, 0.8);// 点击后开始拖动并透明显示
	});
	$(document).unbind("mousemove").bind("mousemove",function(e) {
		if (_move) {
			var x = e.pageX - _x;// 移动时根据鼠标位置计算控件左上角的绝对位置
			var y = e.pageY - _y;
			$dialog.css({
				top : y,
				left : x
			});// 控件新位置
		}
	});
	$(document).unbind("mouseup").bind("mouseup",function() {
		_move = false;
		$dialog.fadeTo("fast", 1);// 松开鼠标后停止移动并恢复成不透明
	});
	$dialog.parent().fadeIn("fast");
}
// //=======抬头提示信息=======================
var smTime;
function showMsg(msg, type) {
	var $alertMsg = $(".alertMsg");
	if (type == "error")
		$alertMsg.css({
			"background-color" : "#F2DEDE",
			"border-color" : "red"
		});
	else
		$alertMsg.css({
			"background-color" : "#E0F2E3",
			"border-color" : "#1FAA39"
		});
	$alertMsg.html(msg);
	$alertMsg.stop().hide().fadeIn("slow");
	clearTimeout(smTime);// 及时清除
	smTime = setTimeout(function() {
		$alertMsg.fadeOut("slow");
	}, 5000);
}

// ===toggle交替事件，1.8、1.9已移除============================
$.fn.toggle = function(fn, fn2) {
	var args = arguments, guid = fn.guid || $.guid++, i = 0, toggler = function(
			event) {
		var lastToggle = ($._data(this, "lastToggle" + fn.guid) || 0) % i;
		$._data(this, "lastToggle" + fn.guid, lastToggle + 1);
		event.preventDefault();
		return args[lastToggle].apply(this, arguments) || false;
	};
	toggler.guid = guid;
	while (i < args.length) {
		args[i++].guid = guid;
	}
	return this.click(toggler);
};
//===========================================
var async = true; // 异步
var ajaxbo = true;
// 同步调用Ajax
function sendRequestTB(type,requsetUrl, paramData, callback) {
	async = false;
	sendRequest(type,requsetUrl, paramData, callback);
	async = true;
}
// 异步调用Ajax
function sendRequest(type,requsetUrl, paramData, callback) {
	$(".alertLoad").parent().fadeIn();
	$.ajax({
		cache : false,
		type : type,//post
		url : requsetUrl + "?timestamp=" + new Date().getTime(),
		data : paramData,
		traditional :true,
		async : async,
		success : function(data) {
			try {
				$(".alertLoad").parent().hide();
				callback(data);
				return true;
			} catch (e) {
				alert("解析异常，请稍后再试");
				console.error(e);
			}
		},
		complete : function(XMLHttpRequest, status) {
			$(".alertLoad").parent().hide();
			if (status != "success") {
				ajaxbo = false;
				if (status == "timeout") {
					alert("请求超时，请稍后再试");
				} else if (status == "error") {
					alert("系统异常，请稍后再试");
				}else{
					alert(status);
				}
			}
		}
	});
}
 
// 去除form表单 特殊字符
function baseCheckForm(form) {
	$(form).find("input").each(function() {
		var val = $.trim($(this).val());
		val = val.replace("<", "&lt;");
		val = val.replace(">", "&gt;");
		$(this).val(val);
	});
} 
