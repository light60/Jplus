<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="${ctx}/resource/img/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="${ctx}/resource/css/base.css?v=${v}" />
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="app-mobile-web-app-capable" content="yes">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${ctx}</title>
</head>
<body class="tc lh40">
	<div><a href="${ctx}/mvc/blog/list/1">MVC Demo</a></div>
	<div>AOP DEMO</div>
	<div>IOC DEMO</div>
	<div>RPC DEMO</div>
	<div>ORM DEMO</div>
	<div>JOB DEMO</div>
	<div>MQ DEMO</div>
	<div>ZK DEMO</div>
	<div>J2Cache DEMO</div>
</body>
</html>